#!/usr/bin/env bash

# This script is not meant to be ran directly.
[[ "${0}" != *install* ]] && exit 12

# Install bootloader.
function _install_bootloader() {
	local __ROOT_UUID
	local __SWAP_UUID

	# Determine if using Plymouth.
	function _is_using_plymouth() {
		if grep -Pqs '^HOOKS=\(.*plymouth.*\)$' '/etc/mkinitcpio.conf'; then
			return 0
		fi

		return 1
	}

	# Get UUIDs for root and swap filesystems.
	function _get_filesystem_uuids() {
		local __swap_device
		local -a __swap_devices

		__ROOT_UUID="$(blkid -o value -s UUID "$(df / --output=source | tail -n 1)")"

		readarray -t __swap_devices < <(swapon --show=NAME --noheadings)

		case "${#__swap_devices[@]}" in
		0)
			echo_magenta 'No swap device found. Continuing...'
			return 0
			;;
		1)
			__swap_device="${__swap_devices[0]}"
			;;
		*)
			while true; do
				echo_blue "${#__swap_devices[@]} swap devices were found."
				echo_blue 'Please select the swap device to use for hibernation:'
				select __swap_device in "${__swap_devices[@]}"; do
					while true; do
						echo_blue "You selected '${__swap_device}'. Is this correct? (y/n):"
						read -r
						case "${REPLY}" in
						'y' | 'Y')
							echo_yellow "'${__swap_device}' selected."
							break 2
							;;
						'n' | 'N')
							break
							;;
						esac
					done
				done
			done
			;;
		esac

		__SWAP_UUID="$(blkid -o value -s UUID "${__swap_device}")"
		return 0
	}

	# Set loader option.
	function _set_loader_option() {
		local __config='/boot/loader/loader.conf'
		local __option="${1}"
		local __value="${2}"

		[[ ! -f "${__config}" ]] && touch "${__config}"

		if grep -Pqs "${__option}" "${__config}"; then
			sed -Ei "/${__option}/c\\${__option} ${__value}" "${__config}"
		else
			echo "${__option} ${__value}" >>"${__config}"
		fi

		return 0
	}

	# Install systemd-boot.
	function _install_systemd_boot() {
		local __entries_dir='/boot/loader/entries'
		local __kernel_params=(
			"root=UUID=${__ROOT_UUID}"
			"rw"
		)
		local -A __options
		__options['console-mode']='max'
		__options['default']='arch.conf'
		__options['timeout']=5

		[[ -n "${__SWAP_UUID}" ]] && __kernel_params+=("resume=UUID=${__SWAP_UUID}")
		_is_using_plymouth && __kernel_params+=('quiet' 'splash')

		if ! bootctl install; then
			echo_red 'Failed to install systemd-boot. Exiting...'
			exit 12
		fi

		mkdir -p "${__entries_dir}"

		cat <<-EOF >"${__entries_dir}/arch.conf"
			title Arch Linux
			linux /vmlinuz-linux
			initrd /initramfs-linux.img
			options ${__kernel_params[*]}
		EOF
		cat <<-EOF >"${__entries_dir}/arch-fallback.conf"
			title Arch Linux (fallback)
			linux /vmlinuz-linux
			initrd /initramfs-linux-fallback.img
			options ${__kernel_params[*]}
		EOF

		if pacman -Qqs 'linux-lts' | grep -Pqs '^linux-lts$'; then
			cat <<-EOF >"${__entries_dir}/arch-lts.conf"
				title Arch Linux LTS
				linux /vmlinuz-linux-lts
				initrd /initramfs-linux-lts.img
				options ${__kernel_params[*]}
			EOF
			cat <<-EOF >"${__entries_dir}/arch-lts-fallback.conf"
				title Arch Linux LTS (fallback)
				linux /vmlinuz-linux-lts
				initrd /initramfs-linux-lts-fallback.img
				options ${__kernel_params[*]}
			EOF
		fi

		local __option
		for __option in "${!__options[@]}"; do
			_set_loader_option "${__option}" "${__options["${__option}"]}"
		done

		echo_green 'Installed systemd-boot!'
		return 0
	}

	_get_filesystem_uuids
	_install_systemd_boot

	unset -f _is_using_plymouth
	unset -f _get_filesystem_uuids
	unset -f _set_loader_option
	unset -f _install_systemd_boot
	return 0
}
