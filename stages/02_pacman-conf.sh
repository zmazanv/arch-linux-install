#!/usr/bin/env bash

# This script is not meant to be ran directly.
[[ "${0}" != *install* ]] && exit 2

# Configure and prep pacman for use.
function _configure_pacman() {
	local __CONFIG='/etc/pacman.conf'

	if [[ ! -f "${__CONFIG}" ]]; then
		echo_red "Could not find '${__CONFIG}'. Exiting..."
		exit 2
	fi

	# Enable colored output.
	function _enable_color() {
		sed -Ei '/^#Color$/s/^#//' "${__CONFIG}"

		if grep -Pqs "^Color$" "${__CONFIG}"; then
			echo_green 'Enabled colored output for pacman!'
			return 0
		fi

		echo_red 'Failed to enable colored output for pacman. Exiting...'
		exit 2
	}

	# Enable the multilib repo.
	function _enable_multilib() {
		local -a __multilib_lines

		__multilib_lines[0]="$(
			grep -Pn '^#\[multilib\]$' "${__CONFIG}" |
				cut -d ':' -f 1
		)"
		__multilib_lines[1]="$(("${__multilib_lines[0]}" + 1))"

		sed -Ei "${__multilib_lines[0]}s/^#//" "${__CONFIG}"
		sed -Ei "${__multilib_lines[1]}s/^#//" "${__CONFIG}"

		if ! grep -Pqsz '\[multilib\]\nInclude = /etc/pacman\.d/mirrorlist' "${__CONFIG}"; then
			echo_red 'Failed to enable the multilib repo for pacman. Exiting...'
			exit 2
		fi

		pacman -Syy

		echo_green 'Enabled the multilib repo for pacman!'
		return 0

	}

	_enable_color
	_enable_multilib

	unset -f _enable_color
	unset -f _enable_multilib
	return 0
}
