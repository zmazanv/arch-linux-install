#!/usr/bin/env bash

# This script is not meant to be ran directly.
[[ "${0}" != *install* ]] && exit 10

# Set modules and hooks for mkinitcpio.
function _configure_mkinitcpio() {
	local __CONFIG='/etc/mkinitcpio.conf'
	local -a __HOOKS
	__HOOKS=($(
		grep -P '^HOOKS=\(.*\)' "${__CONFIG}" |
			sed -E 's/^HOOKS=\(//' |
			sed -E 's/\)$//'
	))

	# Make a backup of the original config.
	function _back_up_config() {
		cp "${__CONFIG}" "${__CONFIG}.bak"
		chmod -w "${__CONFIG}.bak"

		return 0
	}

	# Add hook to hooks array after specified hook.
	function _add_hook_after() {
		local -a __new_hooks
		local __previous_hook="${1}"
		local __hook_to_add="${2}"

		if ! echo "${__HOOKS[@]}" | grep -Pqs "${__previous_hook}"; then
			echo_magenta "Hook '${__previous_hook}' not found in mkinitcpio hooks. Skipping adding hook '${__hook_to_add}' and continuing..."
			echo
			return 1
		fi

		local __hook
		for __hook in "${__HOOKS[@]}"; do
			__new_hooks+=("${__hook}")

			if [[ "${__hook}" == "${__previous_hook}" ]]; then
				__new_hooks+=("${__hook_to_add}")
			fi
		done

		__HOOKS=("${__new_hooks[@]}")

		echo_green "Added the '${__hook_to_add}' mkinitcpio hook!"
		echo
		return 0
	}

	# Set mkinitcpio hook needed for hibernation support.
	function _set_hibernation() {
		_add_hook_after 'filesystems' 'resume'
		return 0
	}

	# Set mkinitcpio hook needed for reading LVM block devices.
	function _set_lvm() {
		while true; do
			echo_blue 'Are you using LVM? (y/n):'
			read -r
			case "${REPLY}" in
			'n' | 'N')
				return 1
				;;
			'y' | 'Y')
				break
				;;
			esac
		done

		_add_hook_after 'block' 'lvm2'
		return 0
	}

	# Set mkinitcpio hook needed for booting with Plymouth splash screen.
	function _set_plymouth() {
		while true; do
			echo_blue 'Are you using Plymouth? (y/n):'
			read -r
			case "${REPLY}" in
			'n' | 'N')
				return 1
				;;
			'y' | 'Y')
				command -v plymouth >/dev/null 2>&1 || pacman -S plymouth
				break
				;;
			esac
		done

		_add_hook_after 'udev' 'plymouth'
		return 0
	}

	# Add the Nvidia mkinitcpio modules if the Nvidia proprietary drivers are installed.
	function _set_nvidia_modules_if_installed() {
		local __nvidia_modules=(
			'nvidia'
			'nvidia_modeset'
			'nvidia_uvm'
			'nvidia_drm'
		)

		command -v nvidia-smi >/dev/null 2>&1 || return 1

		sed -Ei "/^MODULES=\(\)$/s/\(\)/(${__nvidia_modules[*]})/" "${__CONFIG}"

		echo_green 'Added Nvidia mkinitcpio modules!'
		return 0
	}

	# Set needed mkinitcpio hooks.
	function _set_hooks() {
		local __hooks_regex='^HOOKS=\(.*\)$'

		_set_hibernation
		_set_lvm
		_set_plymouth

		sed -Ei "/${__hooks_regex}/s/${__hooks_regex}/HOOKS=(${__HOOKS[*]})/" "${__CONFIG}"

		echo_green 'Set mkinitcpio hooks!'
		return 0
	}

	# Generate the initramfs.
	function _generate_initramfs() {
		if mkinitcpio -P; then
			echo_green 'Generated initramfs!'
			return 0
		fi

		echo_red 'Failed to generate initramfs. Exiting...'
		exit 10
	}

	_back_up_config
	_set_nvidia_modules_if_installed
	_set_hooks
	_generate_initramfs

	unset -f _back_up_config
	unset -f _add_hook_after
	unset -f _set_hibernation
	unset -f _set_lvm
	unset -f _set_plymouth
	unset -f _set_nvidia_modules_if_installed
	unset -f _set_hooks
	unset -f _generate_initramfs
	return 0
}
