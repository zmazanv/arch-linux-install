#!/usr/bin/env bash

# This script is not meant to be ran directly.
[[ "${0}" != *install* ]] && exit 8

# Configure primary user and root.
function _set_up_users() {
	local __USERNAME
	local __NAME
	local __GROUPS=(
		'nix-users'
		'realtime'
		'video'
		'wheel'
	)

	# Get username and optionally user comment from user input interactively.
	function _fetch_name() {
		while true; do
			echo_blue "Please enter your username."
			echo_blue "It should only contain characters 'a-z' and nothing else. It will be all lowercase."
			read -r __USERNAME
			__USERNAME="${__USERNAME#"${__USERNAME%%[![:space:]]*}"}"
			__USERNAME="${__USERNAME%"${__USERNAME##*[![:space:]]}"}"
			__USERNAME="${__USERNAME,,}"

			if [[ ! "${__USERNAME}" =~ ^[a-z]+$ ]]; then
				echo_magenta "You entered: '${__USERNAME}', which is invalid. Please try again."
				echo
				continue
			fi

			while true; do
				echo_blue "You entered '${__USERNAME}'. Is this correct? (y/n):"
				read -r
				case "${REPLY}" in
				'y' | 'Y')
					echo_green 'Continuing...'
					echo
					break 2
					;;
				'n' | 'N')
					echo
					break
					;;
				esac
			done
		done

		while true; do
			echo_blue "Please enter your name. Leave empty to skip."
			read -r __NAME
			__NAME="${__NAME#"${__NAME%%[![:space:]]*}"}"
			__NAME="${__NAME%"${__NAME##*[![:space:]]}"}"

			while true; do
				if [[ -z "${__NAME}" ]]; then
					echo_blue "You chose to skip. Is this correct? (y/n):"
				else
					echo_blue "You entered '${__NAME}'. Is this correct? (y/n):"
				fi
				read -r
				case "${REPLY}" in
				'y' | 'Y')
					echo_green 'Continuing...'
					echo
					break 2
					;;
				'n' | 'N')
					echo
					break
					;;
				esac
			done
		done

		return 0
	}

	# Create the user and append to needed groups if available.
	function _create_user() {
		if ! useradd -m "${__USERNAME}"; then
			echo_red "Failed to create user '${__USERNAME}'. Exiting..."
			exit 8
		fi

		if [[ -n "${__NAME}" ]]; then
			usermod -c "${__NAME}" "${__USERNAME}"
		fi

		local __group
		for __group in "${__GROUPS[@]}"; do
			if grep -Pqs "^${__group}:" '/etc/group'; then
				usermod -aG "${__group}" "${__USERNAME}"
			fi
		done

		echo_green "Created user '${__USERNAME}'!"
		return 0
	}

	# Set password for user and for root.
	function _set_passes() {
		while true; do
			echo_blue "Please enter the password for user '${__USERNAME}':"
			if passwd "${__USERNAME}"; then
				echo_green "Password set for user '${__USERNAME}'!"
				echo
				break
			fi

			echo_magenta 'Please try again.'
			echo
		done

		while true; do
			echo_blue 'Please enter the password for the root user:'
			if passwd; then
				echo_green "Password set for root user!"
				echo
				break
			fi

			echo_magenta 'Please try again.'
			echo
		done

		return 0
	}

	# Edit sudoers file.
	function _edit_sudo() {
		visudo
		echo_green 'Sudoers file edited!'
		return 0
	}

	# Grab the setup scripts for the user setup.
	function _copy_user_setup_scripts() {
		local __submodule_dir='./submodules'
		local __user_home_dir="/home/${__USERNAME}"

		local __submodule
		local __copy
		for __submodule in "${__submodule_dir}"/*; do
			__copy="${__user_home_dir}/$(basename "${__submodule}")"

			cp -r "${__submodule}" "${__copy}"
			chown -R "${__USERNAME}" "${__copy}"
		done
		return 0
	}

	_fetch_name
	_create_user
	_set_passes
	_edit_sudo
	_copy_user_setup_scripts

	unset -f _fetch_name
	unset -f _create_user
	unset -f _set_passes
	unset -f _edit_sudo
	unset -f _copy_user_setup_scripts
	return 0
}
