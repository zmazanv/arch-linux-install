#!/usr/bin/env -S bash -u

# This script is not meant to be ran directly.
[[ "${0}" != *install* ]] && exit 7

function _set_up_local_hostname_resolve() {
	local __NSSWITCH_CONF='/etc/nsswitch.conf'
	local __PKGS=('avahi' 'cups' 'cups-pdf' 'nss-mdns')
	local __SRVC=('avahi-daemon.service' 'cups.service')

	# Check if the needed packages are installed.
	function _packages_installed() {
		local -a __missing=()
		local __pkgs

		__pkgs="$(mktemp)"
		pacman -Qq >"${__pkgs}"

		local i
		for i in "${__PKGS[@]}"; do
			grep -Pq "${i}" "${__pkgs}" || __missing+=("${i}")
		done

		rm -f "${__pkgs}"

		if [[ "${#__missing[@]}" -ne 0 ]]; then
			echo_magenta "Packages not installed: ${__missing[*]}"
			return 1
		fi

		return 0
	}

	# Prompt and install missing packages.
	function _install_missing_packages() {
		while true; do
			echo_blue "Would you like to install the missing packages?:"
			read -r

			case "${REPLY}" in
			'y' | 'Y')
				break
				;;
			'n' | 'N')
				return 1
				;;
			esac
		done

		pacman -Syy --needed "${__PKGS[@]}"

		return $?
	}

	# Enable systemd services.
	function _enable_services() {
		systemctl enable "${__SRVC[@]}"

		return $?
	}

	# Back up Name Service Switch configuration file.
	function _back_up_nsswitch_config() {
		cp "${__NSSWITCH_CONF}" "${__NSSWITCH_CONF}.bak" &&
			chmod -w "${__NSSWITCH_CONF}.bak"

		return $?
	}

	# Add 'mdns_minimal [NOTFOUND=return]' to the hosts in the Name Service Switch configuration file.
	function _modify_nsswitch_config() {
		local __add='mdns_minimal [NOTFOUND=return]'
		local __match='(hosts: mymachines) (resolve \[!UNAVAIL=return\] files myhostname dns)'
		local __replace="\1 ${__add} \2"

		if ! grep -Pq "${__match}" /etc/nsswitch.conf; then
			echo_red "Could not match '${__match}' in /etc/nsswitch.conf"
			return 1
		fi

		sed -Ei "s/${__match}/${__replace}/g" /etc/nsswitch.conf

		return 0
	}

	_packages_installed || { _install_missing_packages || exit 7; }
	_back_up_nsswitch_config || exit 7
	_modify_nsswitch_config || exit 7
	_enable_services || exit 7

	unset -f _packages_installed
	unset -f _install_missing_packages
	unset -f _enable_services
	unset -f _back_up_nsswitch_config
	unset -f _modify_nsswitch_config
	return 0
}
