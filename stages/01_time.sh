#!/usr/bin/env bash

# This script is not meant to be ran directly.
[[ "${0}" != *install* ]] && exit 1

# Set the system timezone interactively and sync the clocks.
function _set_timezone() {
	local __DIRPATH='./resources/timezones'
	local __REGIONS
	local __ZONE
	local __ZONEPATH

	# Fetch the timezone regions to choose from.
	function _prep() {
		if [[ ! -d "${__DIRPATH}" ]]; then
			echo_red "'${__DIRPATH}' not found. Exiting..."
			exit 1
		fi

		readarray -t __REGIONS < <(
			find "${__DIRPATH}" \
				-maxdepth 1 \
				-regex '.*\.tz$' \
				-type f \
				-execdir basename {} \.tz \; |
				sort
		)

		if [[ "${#__REGIONS[@]}" -eq 0 ]]; then
			echo_red "Nothing was found in ${__DIRPATH}. Exiting..."
			exit 1
		fi

		return 0
	}

	# Select the desired region and timezone interactively.
	function _select() {
		local __region
		local __zone
		local __zones

		while [[ -z "${__region}" ]]; do
			echo_blue "Please select a timezone region:"
			select __region in "${__REGIONS[@]}"; do break; done
		done

		local __region_file="${__DIRPATH}/${__region}.tz"

		if [[ ! -f "${__region_file}" ]]; then
			echo_red "'${__region_file}' not found. Exiting..."
			exit 1
		fi

		readarray -t __zones <"${__region_file}"

		case "${#__zones[@]}" in
		0)
			echo_red "Nothing found in '${__region_file}'. Exiting..."
			exit 1
			;;
		1)
			__zone="${__zones[0]}"
			;;
		*)
			while [[ -z "${__zone}" ]]; do
				echo_blue "Please select a timezone:"
				select __zone in "${__zones[@]}"; do break; done
			done
			;;
		esac

		local __zonepath="/usr/share/zoneinfo/${__zone}"

		if [[ ! -f "${__zonepath}" ]]; then
			echo_red "'${__zonepath}' could not be found. Exiting..."
			exit 1
		fi

		while true; do
			echo_blue "You selected '${__zone}'. Is this correct? (y/n):"
			read -r
			case "${REPLY}" in
			'n' | 'N')
				return 1
				;;
			'y' | 'Y')
				break
				;;
			esac
		done

		__ZONE="${__zone}"
		__ZONEPATH="${__zonepath}"
		return 0
	}

	# Symlink the system timezone file into place.
	function _symlink_timezone() {
		local __localtime='/etc/localtime'

		if ln -sf "${__ZONEPATH}" "${__localtime}"; then
			echo_green "Timezone set to '${__ZONE}'!"
			return 0
		fi

		echo_red "Failed to create symlink to '${__localtime}' from '${__ZONEPATH}'. Exiting..."
		exit 1
	}

	# Sync the hardware clock with the software clock.
	function _sync_hwclock() {
		if hwclock --systohc; then
			echo_green 'Synchronized the hardware clock with the software clock!'
			return 0
		fi

		echo_red 'Failed to sync the hardware clock with the software clock. Exiting...'
		exit 1
	}

	_prep
	while true; do
		if _select; then
			break
		fi
	done
	_symlink_timezone
	_sync_hwclock

	unset -f _prep
	unset -f _select
	unset -f _symlink_timezone
	unset -f _sync_hwclock
	return 0
}
