#!/usr/bin/env bash

# This script is not meant to be ran directly.
[[ "${0}" != *install* ]] && exit 5

# Install packages.
function _install_packages() {
	local __DIRPATH='./resources/packages'
	local __NVIDIA_PKGS="${__DIRPATH}/graphics/nvidia.pkgs"
	local -a __PKGS

	if [[ ! -d "${__DIRPATH}" ]]; then
		echo_red "'${__DIRPATH}' not found. Exiting..."
		exit 5
	fi

	# Select if using Nvidia graphics.
	function _select_nvidia() {
		while true; do
			echo_blue 'Would you like to install the Nvidia proprietary drivers? (y/n):'
			read -r
			case "${REPLY}" in
			'n' | 'N')
				break
				;;
			'y' | 'Y')
				if [[ -f "${__NVIDIA_PKGS}" ]]; then
					echo_yellow 'The Nvidia proprietary drivers will be installed...'
					return 0
				fi

				while true; do
					echo_red "The Nvidia package list, '${__NVIDIA_PKGS}', was not found."
					echo_blue "Would you like to proceed without them? (y/n):"
					read -r
					case "${REPLY}" in
					'n' | 'N')
						echo_red "Exiting..."
						exit 5
						;;
					'y' | 'Y')
						break 2
						;;
					esac
				done
				;;
			esac
		done

		echo_yellow 'The Nvidia proprietary drivers will not be installed...'
		return 1
	}

	# Parse the packages to install.
	function _fetch_pkgs() {
		if _select_nvidia; then
			readarray -t __PKGS < <(
				find "${__DIRPATH}" \
					-type f \
					-regex '.*\.pkgs$' \
					-execdir cat {} + |
					uniq
			)
		else
			readarray -t __PKGS < <(
				find "${__DIRPATH}" \
					-type f \
					-regex '.*\.pkgs$' \
					-not -path "${__NVIDIA_PKGS}" \
					-execdir cat {} + |
					uniq
			)
		fi

		if [[ "${#__PKGS[@]}" -eq 0 ]]; then
			echo_red "No packages were found in '${__DIRPATH}'. Exiting..."
			exit 5
		fi

		return 0
	}

	# Install packages interactively.
	function _install_pkgs() {
		while true; do
			echo_yellow 'Interactive package installation starting in 3 seconds...'
			sleep 3
			if pacman -S --needed "${__PKGS[@]}"; then
				echo_green 'Finished package installation!'
				return 0
			fi

			while true; do
				echo_magenta 'The package installation failed.'
				echo_blue 'Would you like to rerun it? (y/n):'
				read -r
				case "${REPLY}" in
				'n' | 'N')
					echo_red 'Exiting...'
					exit 5
					;;
				'y' | 'Y')
					break
					;;
				esac
			done
		done
	}

	_fetch_pkgs
	_install_pkgs

	unset -f _select_nvidia
	unset -f _fetch_pkgs
	unset -f _install_pkgs
	return 0
}
