#!/usr/bin/env bash

# This script is not meant to be ran directly.
[[ "${0}" != *install* ]] && exit 3

# Set the system locale.
function _set_locale() {
	local __LOCALE_GEN='/etc/locale.gen'

	if [[ ! -f "${__LOCALE_GEN}" ]]; then
		echo_red "Unable to find '${__LOCALE_GEN}'. Exiting..."
		exit 3
	fi

	# Uncomment and generate the system locales.
	function _gen_locales() {
		sed -Ei '/^#en_US.UTF-8 UTF-8/s/^#//' "${__LOCALE_GEN}"
		sed -Ei '/^#es_MX.UTF-8 UTF-8/s/^#//' "${__LOCALE_GEN}"
		sed -Ei '/^#es_ES.UTF-8 UTF-8/s/^#//' "${__LOCALE_GEN}"

		if locale-gen; then
			echo_green 'Generated the system locales!'
			return 0
		fi

		echo_red 'Failed to generate the system locales. Exiting...'
		exit 3
	}

	# Create the system locale conf.
	function _create_locale_conf() {
		local __locale_conf='/etc/locale.conf'
		local __vconsole='/etc/vconsole.conf'

		cat <<-'EOF' >>"${__locale_conf}"
			LANG=es_MX.UTF-8
			LANGUAGE=es_MX:es:en_US:en:C
		EOF

		echo 'KEYMAP=la-latin1' >>"${__vconsole}"

		echo_green "Created '${__locale_conf}' and '${__vconsole}'!"
		return 0
	}

	_gen_locales
	_create_locale_conf

	unset -f _gen_locales
	unset -f _create_locale_conf
	return 0
}
