#!/usr/bin/env bash

# This script is not meant to be ran directly.
[[ "${0}" != *install* ]] && exit 9

# Set options for kernel modules and parameters.
function _set_modules_and_params() {
	local __MODPROBE_DIR='/etc/modprobe.d'
	local __SYSCTL_DIR='/etc/sysctl.d'

	# Set options for Nvidia kernel modules if installed.
	function _configure_nvidia_if_installed() {
		local __nvidia_drm_modeset_option='options nvidia_drm modeset=1'
		local __nvidia_drm_fbdev_option='options nvidia_drm fbdev=1'
		local __nvidia_power_option='options nvidia NVreg_PreserveVideoMemoryAllocations=1'

		command -v nvidia-smi >/dev/null 2>&1 || return 1

		cat <<-EOF >"${__MODPROBE_DIR}/nvidia-drm.conf"
			${__nvidia_drm_modeset_option}
			${__nvidia_drm_fbdev_option}
		EOF
		echo "${__nvidia_power_option}" >"${__MODPROBE_DIR}/nvidia-power-management.conf"

		echo_green 'Set Nvidia kernel module options!'
		return 0
	}

	# Increase the value of the 'vm.max_map_count' kernele parameter.
	function _increase_memory_map_count() {
		echo 'vm.max_map_count=2147483642' >"${__SYSCTL_DIR}/01-vm.conf"

		echo_green "Increased value of 'vm.max_map_count'!"
		return 0
	}

	_configure_nvidia_if_installed
	_increase_memory_map_count

	unset -f _configure_nvidia_if_installed
	unset -f _increase_memory_map_count
	return 0
}
