#!/usr/bin/env bash

# This script is not meant to be ran directly.
[[ "${0}" != *install* ]] && exit 11

# Copy pacman hooks.
function _copy_pacman_hooks() {
	local __HOOKS_DIR='/etc/pacman.d/hooks'
	local __DIRPATH='./resources/pacman-hooks'
	local -a __HOOKS

	mkdir -p "${__HOOKS_DIR}"

	if [[ ! -d "${__DIRPATH}" ]]; then
		echo_magenta "'${__DIRPATH}' not found. No pacman hooks to add. Continuing..."
		return 0
	fi

	readarray -t __HOOKS < <(
		find "${__DIRPATH}" \
			-type f \
			-regex '.*\.hook$'
	)

	if [[ "${#__HOOKS[@]}" -eq 0 ]]; then
		echo_magenta "No hooks found in '${__DIRPATH}'. Continuing..."
		return 0
	fi

	cp "${__HOOKS[@]}" "${__HOOKS_DIR}"

	echo_green "Copied ${#__HOOKS[@]} pacman hooks to '${__HOOKS_DIR}'!"
	return 0
}
