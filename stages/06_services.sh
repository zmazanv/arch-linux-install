#!/usr/bin/env bash

# This script is not meant to be ran directly.
[[ "${0}" != *install* ]] && exit 6

# Enable and configure systemd services. Nothing will be done if no services are found.
function _configure_services() {
	local __DIRPATH='resources/services'
	local __NVIDIA_SERVICES="${__DIRPATH}/graphics/nvidia.srvc"
	local -a __SERVICES

	if [[ ! -d "${__DIRPATH}" ]]; then
		echo_magenta "'${__DIRPATH}' not found. Nothing to configure. Continuing..."
		return 0
	fi

	# Determine if Nvidia proprietary drivers are installed.
	function _is_nvidia_installed() {
		command -v nvidia-smi >/dev/null 2>&1 || return 1

		if [[ ! -f "${__NVIDIA_SERVICES}" ]]; then
			echo_magenta "The Nvidia services list, '${__NVIDIA_SERVICES}', was not found. Continuing..."
		fi

		return 0
	}

	# Parse the services to enable. Return non-zero if no services are found.
	function _fetch() {
		if _is_nvidia_installed; then
			readarray -t __SERVICES < <(
				find "${__DIRPATH}" \
					-type f \
					-regex '.*\.srvc$' \
					-execdir cat {} + |
					uniq
			)
		else
			readarray -t __SERVICES < <(
				find "${__DIRPATH}" \
					-type f \
					-regex '.*\.srvc$' \
					-not -path "${__NVIDIA_SERVICES}" \
					-execdir cat {} + |
					uniq
			)
		fi

		if [[ "${#__SERVICES[@]}" -eq 0 ]]; then
			echo_magenta "No services were found in '${__DIRPATH}'. Continuing..."
			return 1
		fi

		return 0
	}

	# Enable services. Ignore any non-existent units.
	function _enable() {
		systemctl enable "${__SERVICES[@]}" 2>/dev/null

		echo_green 'Enabled systemd services!'
		return 0
	}

	# Set service settings for reflector and sshd if installed.
	function _configure() {
		local __reflector_config='/etc/xdg/reflector/reflector.conf'
		local __sshd_config='/etc/ssh/sshd_config'
		local __ssh_options_to_disable=(
			'PasswordAuthentication'
			'PermitRootLogin'
		)

		if command -v reflector >/dev/null 2>&1; then
			if [[ -f "${__reflector_config}" ]]; then
				mv "${__reflector_config}" "${__reflector_config}.bak"
				chmod -w "${__reflector_config}.bak"
			fi

			cat <<-'EOF' >"${__reflector_config}"
				--save /etc/pacman.d/mirrorlist
				--protocol https
				--country 'Canada,Mexico,United States'
				--latest 5
				--sort rate
			EOF

			echo_green 'Created reflector config!'
		fi

		if command -v ssh >/dev/null 2>&1 && [[ -f "${__sshd_config}" ]]; then
			cp "${__sshd_config}" "${__sshd_config}.bak"
			chmod -w "${__sshd_config}.bak"

			local __option
			for __option in "${__ssh_options_to_disable[@]}"; do
				if grep -Pqs "${__option}" "${__sshd_config}"; then
					sed -Ei "s/^.*${__option}.*$/${__option} no/" "${__sshd_config}"
				else
					echo "${__option} no" >>"${__sshd_config}"
				fi

				if ! grep -Pqs "^.*${__option} no.*$" "${__sshd_config}"; then
					echo_red 'Failed to configure sshd. Exiting...'
					exit 6
				fi
			done

			echo_green 'Configured sshd!'
		fi

		return 0
	}

	# Generate the manpage cache.
	function _gen_man_page_cache() {
		command -v mandb >/dev/null 2>&1 || return 1

		if systemctl start man-db.service; then
			echo_green 'Generated manpage cache!'
			return 0
		fi

		echo_magenta 'Failed to generate manpage cache. Continuing...'
		return 1
	}

	_configure
	_fetch && _enable
	_gen_man_page_cache

	unset -f _is_nvidia_installed
	unset -f _fetch_services
	unset -f _enable_services
	unset -f _configure
	unset -f _gen_man_page_cache
	return 0
}
