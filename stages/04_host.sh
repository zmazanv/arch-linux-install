#!/usr/bin/env bash

# This script is not meant to be ran directly.
[[ "${0}" != *install* ]] && exit 4

# Configure host related system settings.
function _configure_host() {
	local __HOSTNAME

	function _get_name() {
		local __hostname

		while [[ -z "${__hostname}" ]]; do
			echo_blue 'Please enter your hostname:'
			read -r __hostname
		done

		while true; do
			echo_blue "You entered '${__hostname}'. Is this correct? (y/n):"
			read -r
			case "${REPLY}" in
			'n' | 'N')
				return 1
				;;
			'y' | 'Y')
				break
				;;
			esac
		done

		__HOSTNAME="${__hostname}"
		return 0
	}

	# Set hostname and configure localhost.
	function _set_host() {
		echo "${__HOSTNAME}" >>'/etc/hostname'
		cat <<-EOF >>'/etc/hosts'
			127.0.0.1 localhost
			::1 localhost
			127.0.1.1 ${__HOSTNAME}
		EOF

		echo_green "Configured localhost and set hostname to ${__HOSTNAME}!"
		return 0
	}

	while true; do
		if _get_name; then
			break
		fi
	done
	_set_host

	unset -f _get_name
	unset -f _set_host
	return 0
}
