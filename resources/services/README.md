# Services

These are just text files with systemd units listed.

They are used by `_configure_services` in `../stages/06_services.sh`.
