# Packages

These are just text files with Arch package names listed.

They are used by `_install_packages` in `../stages/05_packages.sh`.
