# Timezones

This directory just holds listings of the available timezones at `/usr/share/zoneinfo`.

They are used by `_select_timezone` from `../stages/01_time.sh` to select from when setting the system timezone.
